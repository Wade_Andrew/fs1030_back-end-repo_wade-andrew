import express from "express";
import jwt from "express-jwt";
import * as db from "./dataHandler";
import { v4 as uuidv4 } from "uuid";

const router = express.Router();

const validateEntries = (req, res, next) => {
  const body = req.body;
  const invalid = [];
  console.log(body);
  for (let property in body) {
    console.log(property);
    if (body[property] == null || body[property].length === 0) {
      invalid.push(property);
    }
  }
  const requiredProperties = ["name", "email", "phoneNumber", "content"];
  requiredProperties
    .filter((prop) => !body.hasOwnProperty(prop))
    .forEach((key) => invalid.push(key));
  if (invalid.length > 0) {
    return res.status(400).send({ message: "validation error", invalid });
  }
  next();
};

router.post("/", validateEntries, async (req, res) => {
  const body = req.body;
  const newEntry = {
    id: uuidv4(),
    name: body.name,
    email: body.email,
    phoneNumber: body.phoneNumber,
    content: body.content,
  };
  console.log(newEntry, req.body);
  await db.addEntry(newEntry);
  return res.send(newEntry);
});

router.use(jwt({ secret: process.env.JWT_SECRET }));

router.get("/", async (req, res) => {
  res.send(await db.getAllEntries());
});

export default router;

{
  /* const emailRegex =
  /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
const validateEmail = (email) => {
  let validEmail = emailRegex.test(email);
  if (!validEmail) return false;
  if (email.length > 254) return false;
  else return true;
}; */
}

{
  /* const validateEntries = (req, res, next) => {
  const email = req.body.email;
  const invalid = [];
  if (req.body.name == null) {
    invalid.push("Please enter your name");
  }
  if (req.body.email == null || !validateEmail(email)) {
    invalid.push("Please enter a valid email address");
  }
  if (req.body.phoneNumber == null) {
    invalid.push("Please enter your phone number");
  }
  if (req.body.content == null) {
    invalid.push("Please fill out the message field");
  }
  if (invalid.length > 0) {
    return res.status(400).json({ invalid });
  }
  next()
}; */
}
