import { promises as fs } from "fs";
import path from "path";

const entriesFile = path.resolve("./data/db_entries.json");
const usersFile = path.resolve("./data/db_users.json");

const writeEntry = async (data) => {
  await fs.writeFile(entriesFile, JSON.stringify(data));
};

const writeUser = async (data) => {
  await fs.writeFile(usersFile, JSON.stringify(data));
};

const addEntry = async (data) => {
  try {
    let content = await getAllEntries();
    content.push(data);
    writeEntry(content);
    console.log("file written");
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const getAllEntries = async () => {
  try {
    let content = await fs.readFile(entriesFile);
    return JSON.parse(content);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const addUser = async (data) => {
  try {
    let content = await getAllUsers();
    content.push(data);
    writeUser(content);
    console.log("file written");
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const getAllUsers = async () => {
  try {
    let content = await fs.readFile(usersFile);
    return JSON.parse(content);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export { addEntry, getAllEntries, addUser, getAllUsers };
